﻿using System;
using System.Net.Mail;
using MailManager.Helpers;
using NUnit.Framework;

namespace MailManager.Tests.Helpers
{
    [TestFixture]
    public class MailMessageHelpersTests
    {
        [Test]
        public void IsMailMessageValid_MessageNull_ShouldReturnFalse()
        {
            var result = MailMessageHelpers.IsMailMessageValid(null, out Exception validationException);
            Assert.IsFalse(result);
            Assert.IsInstanceOf<ArgumentNullException>(validationException);
            Assert.AreEqual(validationException.Message, "message cannot be null");
        }

        [Test]
        public void IsMailMessageValid_MessageHasToEmpty_ShouldReturnFalse()
        {
            MailMessage mailMessage = new MailMessage();
            var result = MailMessageHelpers.IsMailMessageValid(mailMessage, out Exception validationException);
            Assert.IsFalse(result);
            Assert.IsInstanceOf<ArgumentException>(validationException);
            Assert.AreEqual(validationException.Message, "To field cannot be null or empty");
        }

        [Test]
        public void IsMailMessageValid_MessageHasFromEmpty_ShouldReturnFalse()
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add("to@valid.it");
            var result = MailMessageHelpers.IsMailMessageValid(mailMessage, out Exception validationException);
            Assert.IsFalse(result);
            Assert.IsInstanceOf<ArgumentException>(validationException);
            Assert.AreEqual(validationException.Message, "From field cannot be null or empty");
        }

        [Test]
        public void IsMailMessageValid_MessageHasNoSubject_ShouldReturnFalse()
        {
            MailMessage mailMessage = new MailMessage("from@from.it","to@to.it");
            var result = MailMessageHelpers.IsMailMessageValid(mailMessage, out Exception validationException);
            Assert.IsFalse(result);
            Assert.IsInstanceOf<ArgumentException>(validationException);
            Assert.AreEqual(validationException.Message, "Subject cannot be null or empty");
        }
    }
}