﻿using System;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Threading;
using MailManager.Tests._Models;
using NUnit.Framework;

namespace MailManager.Tests
{
    [TestFixture]
    public class MailManagerTests
    {
        [TearDown]
        public void ResetMailManager()
        {
            MailManager.Reset();
        }

        [Test]
        public void Reset_ShouldNotThrow()
        {
            Assert.DoesNotThrow(MailManager.Reset);
        }

        [Test]
        public void Reset_ShouldReset()
        {
            MailManager.Reset();
            Assert.IsFalse(MailManager.IsInitialized);
            Assert.IsFalse(MailManager.IsWorking);
        }

        [Test]
        public void Initialize_SmtpNull_ShouldThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(
                () => MailManager.Initialize(null));
        }

        [Test]
        public void Initialize_SmtpNotNull_ShouldInitialize()
        {
            Assert.DoesNotThrow(
                () => MailManager.Initialize(new SmtpFakeClass()));
        }

        [Test]
        public void Initialize_ValidParameters_ShouldInitialize()
        {
            MailManager.Initialize(new SmtpFakeClass());
            Assert.IsTrue(MailManager.IsInitialized);
        }

        [Test]
        public void Initialize_SmtpNull_ShouldNotInitialize()
        {
            try
            {
                MailManager.Initialize(null);
            }
            catch (Exception e)
            {
                //Ci aspettiamo che lanci un eccezione in questo caso
            }

            Assert.IsFalse(MailManager.IsWorking);
        }

        [Test]
        public void Enqueue_MessageNull_ShouldThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(
                () => MailManager.Enqueue(null));
        }

        [Test]
        public void Enqueue_MessageNotNull_ShouldEnqueueWithoutThrowing()
        {
            var mailMessage = new MailMessage("from@valid.it",
                "to@valid.it", "subject", "body");
            Assert.DoesNotThrow(() => MailManager.Enqueue(mailMessage));
        }

        [Test]
        public void Enqueue_MessageNotValid_ShouldThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => MailManager.Enqueue(new MailMessage()));
        }

        [Test]
        public void Start_ShouldNotThrow()
        {
            Assert.DoesNotThrow(MailManager.Start);
        }

        [Test]
        public void Start_ShouldSetIsWorkingToTrue()
        {
            MailManager.Start();
            Assert.IsTrue(MailManager.IsWorking);
        }

        [Test]
        public void Start_ShouldCallSendEmailAsync()
        {
            var fakeSmtpClass = new SmtpFakeClass();
            MailManager.Initialize(fakeSmtpClass);
            MailManager.Enqueue(new MailMessage("from@from.it",
                "to@valid.it", "subject", "body"));
            MailManager.Start();

            //Aspettiamo che il manager sia effettivamente partito
            Thread.Sleep(5000);

            Assert.IsTrue(fakeSmtpClass.IsSendMailAsyncBeingCalled);
        }

        [Test]
        public void Stop_WithoutResetting_ShouldStop()
        {
            MailManager.Start();
            Thread.Sleep(5000);

            MailManager.Stop();
            Assert.IsFalse(MailManager.IsWorking);
        }

        [Test]
        public void Stop_WithResetting_ShouldStopAndReset()
        {
            MailManager.Initialize(new SmtpFakeClass());
            MailManager.Start();
            Thread.Sleep(5000);

            MailManager.Stop(reinitialize: true);
            Assert.IsFalse(MailManager.IsInitialized);
            Assert.IsFalse(MailManager.IsWorking);
        }

        [Test]
        public void Working_AnyMail_ShouldSendEmail()
        {
            var smtpClient = new SmtpFakeClass();
            var loggingContext = new FakeMailLoggingContext();

            string from = "from@from.it";
            string to = "to@to.it";
            string subject = "subject@subject.it";
            string body = "body@body.it";

            var mailMessage = new MailMessage(
                from, to, subject, body);

            MailManager.Initialize(smtpClient, loggingContext);
            MailManager.Enqueue(mailMessage);
            MailManager.Start();

            Thread.Sleep(5000);

            Assert.IsTrue(smtpClient.IsSendMailAsyncBeingCalled);
            Assert.IsTrue(loggingContext.HasCalledCreateNewEmail);

            Assert.AreEqual(from, loggingContext.CreateNewEmailFrom);
            Assert.AreEqual(to, loggingContext.CreateNewEmailTo);
            Assert.AreEqual(subject, loggingContext.CreateNewEmailSubject);
            Assert.AreEqual(body, loggingContext.CreateNewEmailBody);
        }

        [Test]
        public void Working_AnyMail_ShouldSendEmailAndSetSuccessStatus()
        {
            var smtpClient = new SmtpFakeClass();
            var loggingContext = new FakeMailLoggingContext();

            string from = "from@from.it";
            string to = "to@to.it";
            string subject = "subject@subject.it";
            string body = "body@body.it";

            var mailMessage = new MailMessage(
                from, to, subject, body);

            MailManager.Initialize(smtpClient, loggingContext);
            MailManager.Enqueue(mailMessage);
            MailManager.Start();

            Thread.Sleep(5000);

            Assert.IsTrue(smtpClient.IsSendMailAsyncBeingCalled);
            Assert.IsTrue(loggingContext.HasCalledSetQueuedDate);
            Assert.IsTrue(loggingContext.HasCalledSetSentDate);
            Assert.IsTrue(loggingContext.HasCalledCreateNewEmail);
            Assert.IsTrue(loggingContext.HasCalledSetStatusSuccess);

            Assert.AreEqual(from, loggingContext.CreateNewEmailFrom);
            Assert.AreEqual(to, loggingContext.CreateNewEmailTo);
            Assert.AreEqual(subject, loggingContext.CreateNewEmailSubject);
            Assert.AreEqual(body, loggingContext.CreateNewEmailBody);

            Assert.AreEqual(FakeMailLoggingContext.GENERIC_GUID,
                loggingContext.SetStatusSuccessGuid);
            Assert.AreEqual(FakeMailLoggingContext.GENERIC_GUID,
                loggingContext.SetQueuedTimeGuid);
            Assert.AreEqual(FakeMailLoggingContext.GENERIC_GUID,
                loggingContext.SetSentDateGuid);
        }

        [Test]
        public void Woring_AnyMailThrow_ShouldNotThrow()
        {
            var exceptionToThrow = new Exception("ex_to_throw_smtp");
            var smtpClient = new SmtpFakeClass(exceptionToThrow);
            var loggingContext = new FakeMailLoggingContext();

            string from = "from@from.it";
            string to = "to@to.it";
            string subject = "subject@subject.it";
            string body = "body@body.it";

            var mailMessage = new MailMessage(
                from, to, subject, body);

            MailManager.Initialize(smtpClient, loggingContext);
            MailManager.Enqueue(mailMessage);
            MailManager.Start();

            Thread.Sleep(5000);

            Assert.IsTrue(smtpClient.IsSendMailAsyncBeingCalled);
            Assert.IsTrue(loggingContext.HasCalledSetQueuedDate);
            Assert.IsTrue(loggingContext.HasCalledCreateNewEmail);
            Assert.IsFalse(loggingContext.HasCalledSetStatusSuccess);
            Assert.IsFalse(loggingContext.HasCalledSetSentDate);
            Assert.IsTrue(loggingContext.HasCalledSetStatusError);

            Assert.AreEqual(FakeMailLoggingContext.GENERIC_GUID, 
                loggingContext.SetStatusErrorGuid);
            
            Assert.AreEqual(exceptionToThrow.Message, 
                loggingContext.SetStatusErrorException.Message); 
            Assert.AreEqual(FakeMailLoggingContext.GENERIC_GUID,
                loggingContext.SetQueuedTimeGuid);           
        }
    }
}