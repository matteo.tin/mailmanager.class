﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;
using MailManager.Interfaces;

namespace MailManager.Tests._Models
{
    public class SmtpFakeClass : ISmtpClient
    {
        private readonly Exception exToThrow;

        public SmtpFakeClass(Exception exToThrow = null)
        {
            this.exToThrow = exToThrow;
        }

        public bool IsSendMailAsyncBeingCalled;

        public async Task SendMailAsync(MailMessage mailMessage)
        {
            IsSendMailAsyncBeingCalled = true;
            if (this.exToThrow != null) throw this.exToThrow;
        }
    }
}