﻿using System;
using System.Threading.Tasks;
using MailManager.Interfaces;

namespace MailManager.Tests._Models
{
    public class FakeMailLoggingContext: IMailManagerLogContext
    {
        public static readonly Guid GENERIC_GUID = Guid.Parse("6AE3E752-46B2-4D77-87AB-8D1D9D52B15A");
        public bool HasCalledCreateNewEmail;
        public string CreateNewEmailFrom;
        public string CreateNewEmailTo;
        public string CreateNewEmailSubject;
        public string CreateNewEmailBody;
        
        public bool HasCalledSetStatusSuccess;
        public Guid SetStatusSuccessGuid;
        
        public bool HasCalledSetStatusError;
        public Guid SetStatusErrorGuid;
        public Exception SetStatusErrorException;

        public bool HasCalledSetSentDate;
        public Guid SetSentDateGuid;

        public bool HasCalledSetQueuedDate;
        public Guid SetQueuedTimeGuid;
        public async Task<Guid> CreateNewEmail(string @from, string to, string subject, string body)
        {
            HasCalledCreateNewEmail = true;
            CreateNewEmailFrom = from;
            CreateNewEmailTo = to;
            CreateNewEmailSubject = subject;
            CreateNewEmailBody = body;
            
            return GENERIC_GUID;
        }

        public async Task SetStatusSuccess(Guid id)
        {
            SetStatusSuccessGuid = id;
            HasCalledSetStatusSuccess = true;
        }

        public async Task SetStatusError(Guid id, Exception ex)
        {
            SetStatusErrorGuid = id;
            SetStatusErrorException = ex;
            HasCalledSetStatusError = true;
        }

        public async Task SetSentDate(Guid guid, DateTime utcNow)
        {
            HasCalledSetSentDate = true;
            SetSentDateGuid = guid;
        }

        public async Task SetQueuedTime(Guid guid, DateTime time)
        {
            HasCalledSetQueuedDate = true;
            SetQueuedTimeGuid = guid;
        }
    }
}