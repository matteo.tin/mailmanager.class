﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using MailManager.Helpers;
using MailManager.Interfaces;
using MailManager.Models;

namespace MailManager
{
    public static class MailManager
    {
        private static ISmtpClient smtpClient;
        private static IMailManagerLogContext context;
        private static Queue<Message> mailMessages;

        /// <summary>
        /// Questo flag indica se il manager è in lavoro oppure
        /// no
        /// </summary>
        public static bool IsWorking { get; private set; }

        /// <summary>
        /// Indica se il manager corrente è stato già inizializzato
        /// </summary>
        public static bool IsInitialized { get; private set; }

        /// <summary>
        /// Inizializza il manager e lo mette in ascolto
        /// </summary>
        /// <param name="client">
        /// E' il client SMTP di System.Net
        /// </param>
        /// <param name="context">
        /// Tramite questa interfaccia viene gestito il logging delle mail
        /// se lasciato a null, il logging risulta disabilitato
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Restituisce questa eccezione quando il client SMTP è null
        /// </exception>
        public static void Initialize(ISmtpClient client,
            IMailManagerLogContext context = null)
        {
            //Non può esistere un manager con il client a null
            if (client == null) throw new ArgumentNullException("smtp_client_null");
            MailManager.smtpClient = client;
            MailManager.context = context;

            IsInitialized = true;
        }

        /// <summary>
        /// Mette in coda un nuovo messaggio per essere inviato
        /// </summary>
        /// <param name="message">Il messaggio da mettere in coda</param>
        /// <exception cref="ArgumentNullException">
        /// Restitusce questa eccezione quando il messaggio da mettere in coda
        /// è null
        /// </exception>
        public static void Enqueue(MailMessage message)
        {
            if (message == null) throw new ArgumentNullException("message_cannot_be_null");
            if (!MailMessageHelpers.IsMailMessageValid(message, out Exception validationException))
                throw new ArgumentException("mail message is not valid", validationException);

            //Se la lista dei messaggi da spedire risulta essere null, la valorizziamo
            if (mailMessages == null) mailMessages = new Queue<Message>();

            mailMessages.Enqueue(StoreNewEmailAsync(message).Result);
        }

        /// <summary>
        /// Ripristina lo stato iniziale del manager
        /// </summary>
        public static void Reset()
        {
            smtpClient = null;
            IsWorking = false;
            mailMessages = null;
            IsInitialized = false;
        }

        /// <summary>
        /// Lancia il manager che rimarra in ascolto delle nuove mail e le invierà
        /// in background
        /// </summary>
        public static void Start()
        {
            IsWorking = true;
            Task.Run(WorkingTaskAsync);
        }

        /// <summary>
        /// Ferma, e all'occorenza resetta, il manager
        /// </summary>
        /// <param name="reinitialize">Se impostato a true, resetta il manager</param>
        public static void Stop(bool reinitialize = false)
        {
            IsWorking = false;
            if (reinitialize) Reset();
        }

        private static async Task WorkingTaskAsync()
        {
            while (IsWorking)
            {
                var message = mailMessages.Dequeue();
                if (message != null)
                {
                    try
                    {
                        await smtpClient.SendMailAsync(message.MailMessage);
                        await SetSuccessfulStatus(message);
                    }
                    catch (Exception e)
                    {
                        await SetErrorState(message, e);
                    }
                }

                Thread.Sleep(500);
            }
        }

        private static async Task<Message> StoreNewEmailAsync(MailMessage message)
        {
            Guid guid = Guid.Empty;
            var queuedTime = DateTime.UtcNow;
            if (context != null)
            {
                guid = await context.CreateNewEmail(message.From.ToString(),
                    String.Join(",", message.To),
                    message.Subject,
                    message.Body);

                await context.SetQueuedTime(guid, queuedTime);
            }
            

            var result = new Message(guid, message);
            result.QueuedTime = queuedTime;
            return result;
        }

        private static async Task SetSuccessfulStatus(Message message)
        {
            List<Task> tasks = new List<Task>();
            if (context != null)
            {
                tasks.Add( context.SetStatusSuccess(message.Id));
                tasks.Add(context.SetSentDate(message.Id, DateTime.UtcNow));
            }

            await Task.WhenAll(tasks);
        }

        private static async Task SetErrorState(Message message, Exception ex)
        {
            if (context != null)
                await context.SetStatusError(message.Id, ex);
        }
    }
}