﻿using System;
using System.Net.Mail;

namespace MailManager.Models
{
    internal class Message
    {
        internal Guid Id { get; set; }
        internal DateTime QueuedTime { get; set; }
        internal DateTime SentTime { get; set; }
        internal MailMessage MailMessage { get; }

        public Message(Guid id, MailMessage message)
        {
            this.Id = id;
            MailMessage = message;
        }
        public Message(MailMessage mailMessage)
        {
            MailMessage = mailMessage;
        }
    }
}