﻿using System;
using System.Net.Mail;

namespace MailManager.Helpers
{
    public static class MailMessageHelpers
    {
        public static bool IsMailMessageValid(MailMessage message, 
            out Exception validationException)
        {
            validationException = null;
            
            if (message == null)
            {
                validationException = new ArgumentNullException(message: "message cannot be null", null);
                return false;
            }

            if (message.To.Count == 0)
            {
                validationException = new ArgumentException("To field cannot be null or empty");
                return false;
            }
            
            if (message.From == null)
            {
                validationException = new ArgumentException("From field cannot be null or empty");
                return false;
            }
            
            if (String.IsNullOrWhiteSpace(message.Subject))
            {
                validationException = new ArgumentException("Subject cannot be null or empty");
                return false;
            }
            return true;
        }
    }
}