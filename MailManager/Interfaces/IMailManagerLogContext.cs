﻿using System;
using System.Threading.Tasks;

namespace MailManager.Interfaces
{
    public interface IMailManagerLogContext
    {
        Task<Guid> CreateNewEmail(string from, string to, string subject, string body);
        Task SetStatusSuccess(Guid id);
        Task SetStatusError(Guid id, Exception ex);
        Task SetSentDate(Guid guid, DateTime time);
        Task SetQueuedTime(Guid guid, DateTime time);
    }
}