﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace MailManager.Interfaces
{
    public interface ISmtpClient
    {
        /// <summary>
        /// Invia una mail in modalità asincrona
        /// </summary>
        /// <param name="mailMessage">il messaggio da inviare</param>
        /// <returns></returns>
        Task SendMailAsync(MailMessage mailMessage);
    }
}